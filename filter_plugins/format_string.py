#!/usr/bin/env python
# https://github.com/pallets/jinja/issues/545
from markupsafe import soft_str

def format_string(text, fmt, *kw):
    return soft_str(fmt).format(text, *kw)

class FilterModule(object):
    def filters(self):
        return dict(format_string=format_string)
